<?php
/** 
 * @package Wordpress 
*/

get_header(); ?>

    <section class="jumbotron jumbotron-fluid home_banner home_banner_first">
        <div class="container">
            <div class="home_banner--items">
                <h2 class="display-4">Hola, mundo!</h2>
                <p class="lead">Please, select a static page for your home page.</p>
            </div>
        </div>
    </section>


<?php get_footer(); ?>