<?php

/** 
 * @package WordPress 
*/

get_header(); ?>


<section class="content">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'page' ); ?>
				<?php comments_template(); ?>
                <?php endwhile; ?>
                <?php the_field('tabs-info'); ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer();?>