<?php
/** 
 * @package Wordpress 
*/

// Post thumbnails
add_theme_support( 'post-thumbnails' );
//Apply do_shortcode() to widgets so that shortcodes will be executed in widgets
add_filter( 'widget_text', 'do_shortcode' );

// Allow header menu be customizable in Dashboard
function register_my_menus() {
    register_nav_menus(
      array(
        'header-menu' => __( 'Header Menu' ),
        'map-menu' => __( 'Map Menu' ),
        '5claves-menu' => __( '5 Claves Menu' ),
       )
     );
   }
   add_action( 'init', 'register_my_menus' );


//Widget support for the 3 column footer
if (function_exists('register_sidebar')) {
    register_sidebar(
        array(
        'name' => 'Footer Left',
        'id'   => 'footer-left-widget',
        'description'   => 'Left Footer widget position (put Map Menu here).',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));

    register_sidebar(
        array(
        'name' => 'Footer Center',
        'id'   => 'footer-center-widget',
        'description'   => 'Center Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));

    register_sidebar(
        array(
        'name' => 'Footer Right',
        'id'   => 'footer-right-widget',
        'description'   => 'Right Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));

    register_sidebar(
        array(
        'name' => 'Footer Copyright',
        'id'   => 'footer-copyright-widget',
        'description'   => 'Copyright Footer widget position.',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ));

}

?>