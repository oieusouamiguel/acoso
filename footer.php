<?php
/**
 * @package WordPress
 */
?> 
 
 <footer class="footer_items">
        <div class="container footer_items--content">
            <div class="row">
                <div class="col-lg-2 footer_items--project-map">
                    <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-left-widget') ) ?>                
                </div>
                <div class="col-lg-8 footer_items--claves">
                    <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-center-widget') ) ?>
                </div>
                <div class="col-lg-2 footer_items--support">
                    <div class="footer_support">
                        <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-right-widget') ) ?>
                    </div>
                </div>
            </div>
            <div class="footer_copyright">
                <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('footer-copyright-widget') ) ?>
            </div>
        </div>
    </footer>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/bootstrap.bundle.min.js"></script>
    
    <?php wp_footer(); ?>

</body>
</html>