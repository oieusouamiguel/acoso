<?php

// 404 error page

get_header(); ?>

<section class="jumbotron jumbotron-fluid home_banner home_banner_first">
    <div class="container">
        <div class="home_banner--items">
            <h2 class="display-4">Ops! Essa página não existe.</h2>
        </div>
    </div>
</section>

<?php get_footer(); ?>