<?php
/** 
 * @package WordPress 
*/
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title><?php wp_title('|',true,'right'); ?><?php bloginfo('name'); ?></title>
    
    <meta name="description" content="<?php bloginfo('description'); ?>" />
    
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <header class="header_items">
        <div class="container">
            <nav class="navbar navbar-expand-lg header_navbar" role="navigation">
                <div class="header_navbar--title">
                    <div class="header_navbar--title--items">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/acoso-logo2.png" alt="Ícone">
                        <div>
                            <a href="<?php echo home_url(); ?>">
                                <h1>Pornografía no consentida</h1>
                            </a>
                            <p>Cinco claves para denunciar y resistir su publicación</p>
                        </div>
                    </div>
                    <div class="header_lang">
                        <!-- inserir aqui input com versões de cada país -->
                        <img src="<?php echo get_template_directory_uri(); ?>/images/acoso-logo1.svg" alt="acoso.online">
                        <div class="header-lang-select">
                            <label>País</label>
                            <select>
                                <option value="mexico">México</option>
                                <option value="brasil">Brasil</option>
                                <option value="eua">EUA</option>
                            </select>
                        </div>
                    </div>
                </div>
           
                <div class="navbar-nav header_navbar--items">
                    <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) );?>
                </div>
            </nav>
        </div>
    </header>